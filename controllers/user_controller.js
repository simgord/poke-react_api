let user = require('../models/user_model.js');

let jwtFactory = require('jsonwebtoken');
let properties = require('../config/properties.js');


/**
 * @RequestMapping @POST /user/
 */
exports.createuser = function(req, res){

  const newuser = new user({
      email:            req.body.email,
      username:         req.body.username,
      password:         req.body.password,
      pokemonCaught:    [],
      pokemonSeen:      []
  });
    newuser.save()
    .then(data => {
        res.send(data);
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while creating the user."
        });
    });
}

/**
 * @RequestMapping @GET /user/
 */
exports.getusers = function(req, res){
    user.find()
    .then(user => {
        res.send(user);
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while retrieving user."
        });
    });
}

/**
 * @RequestMapping @GET /user/:id
 */
exports.getuserByID = function(req, res){

    user.findById(req.params.id)
    .then(user => {
        if(!user) {
            return res.status(404).send({
                message: "user not found with id " + req.params.id
            });
        }
        res.send(user);
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "user not found with id " + req.params.id
            });
        }
        return res.status(500).send({
            message: "Error retrieving user with id " + req.params.id
        });
    });

};

/**
 * @RequestMapping @PUT /user/:id
 */
exports.update = (req, res) => {

  user.findByIdAndUpdate(req.params.id, req.body, {new: true})
  .then(user => {
      if(!user) {
          return res.status(404).send({
              message: "user not found with id " + req.params.id
          });
      }
      res.send(user);
  }).catch(err => {
      if(err.kind === 'ObjectId') {
          return res.status(404).send({
              message: "user not found with id " + req.params.id
          });
      }
      return res.status(500).send({
          message: "Error updating user with id " + req.params.id
      });
  });
};

/**
 * @RequestMapping @DELETE /user/
 */
exports.deleteuser = (req, res) => {
    user.findByIdAndRemove(req.params.id)
    .then(user => {
        if(!user) {
            return res.status(404).send({
                message: "user not found with id " + req.params.id
            });
        }
        res.send({message: "user deleted successfully!"});
    }).catch(err => {
        if(err.kind === 'ObjectId' || err.name === 'NotFound') {
            return res.status(404).send({
                message: "user not found with id " + req.params.id
            });
        }
        return res.status(500).send({
            message: "Could not delete user with id " + req.params.id
        });
    });
};

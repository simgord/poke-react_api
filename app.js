let express     = require('express');
let mongoose    = require('mongoose');
let bodyParser  = require('body-parser');
let cors        = require('cors');

let user_route  = require('./routes/user_route.js');
let login_route = require('./routes/login_route.js');
let auth        = require('./utils/token_validation');


let port = process.env.PORT || '3000'

let app = initServer(port);

app.use('/' , login_route);
app.use('/user' , auth.validSession, user_route);

function initServer(port){

    let app = express();

    app.listen(port);
    console.log("Serveur sur le port "+ port);

    console.log("Server démarré !");

    const database_config = require('./config/database_config')
    mongoose.connect(database_config.url, {useNewUrlParser: true});
    console.log("MongoServer, connexion à la database : " + database_config.url);
    console.log("Vous pouvez désormais utilisez l'API");

    app.use(bodyParser.urlencoded({extended: false}));
    app.use(bodyParser.json());

    // console.log("Enabling other domains access...")
    app.use(cors());
    // console.log("Ok");

    app.get('/', function (req, res) {
      res.send('Express API : poke-react')
    })
    return app;

}

module.exports = app;
